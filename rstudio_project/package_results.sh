#!/bin/bash

# Note: pandoc>1.12.3 must be available on the PATH
# Otherwise set RSTUDIO_PANDOC environment variable
# Try `Sys.getenv("RSTUDIO_PANDOC")` from Rstudio to find path

RSTUDIO_PANDOC_LINUX="/usr/lib/rstudio/bin/pandoc"
RSTUDIO_PANDOC_MACOS="/Applications/RStudio.app/Contents/MacOS/pandoc"

if [ -f "$RSTUDO_PANDOC_LINUX"]; then
  export RSTUDIO_PANDOC="$RSTUDIO_PANDOC_LINUX"
elif [ -f "$RSTUDIO_PANDOC_MACOS" ]; then
  export RSTUDIO_PANDOC="$RSTUDIO_PANDOC_MACOS"
fi

git diff-index --quiet HEAD --
if [ $? -ne 0 ]; then
  echo "Uncommitted changes exist, exiting!"
else
  REV=$(git rev-parse --short HEAD)
  NOW=$(date +"%Y-%m-%d")
  DIRNAME="$( basename "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" )"
  OUTPUT_DIRNAME="${DIRNAME}_results_${NOW}_${REV}"
  echo "${OUTPUT_DIRNAME}"
  Rscript --vanilla "run_analysis.R" "${OUTPUT_DIRNAME}"
  if [ $? -ne 0 ]; then
    echo "Error executing 'run_analysis.R', exiting!"
  fi
  tar czvf "${OUTPUT_DIRNAME}.tar.gz" -C "results" "${OUTPUT_DIRNAME}"
fi
