#' Generate alignment summary table from a directory of alignment logs
#'
#' @param alignment_log_files List of the alignment log files to summarize
#' 
#' @param aligner The aligner used (currently supports STAR or Tophat)
#'
#' @return A tibble with sample, input, mapped, multi_align, and many_align read
#'   number extracted from the summary
#' 
#' @examples
#' star_summary_files <- Sys.glob(file.path('results/star_mapping/', "*.Log.final.out"))
#' alignment_summary_table <- alignment_summary(star_summary_files, 'STAR')
alignment_summary <- function(alignment_log_files, format) {
    if (format == 'Tophat') {
        func = tophat_se_alignment_summary
    } else if (format == 'STAR') {
        func = star_se_alignment_summary
    } else {
        stop(sprintf('Unknown format specified: %s', format))
    }
    alignment_summary <- cbind(do.call(rbind, lapply(alignment_log_files, func)))
    return(alignment_summary)
}


#' Parse Tophat single-end alignment summary
#'
#' @param tophat_summary_file Path to a tophat summary file
#'
#' @return A tibble with input, mapped, multi_align, and many_align read
#'   number extracted from the summary
#' 
#' @examples
#' tophat_se_alignment_summary('tophat_output_summary.txt')
#' 
tophat_se_alignment_summary <- function(tophat_summary_file) {
    library(readr)
    library(stringr)
    library(tibble)
    
    all_data = read_lines(tophat_summary_file)
    input_line <- all_data %>% 
        str_subset("Input +") %>%
        str_split_fixed(":", 2)
    input_reads <- as.numeric(str_trim(input_line[,2]))
    
    mapped_line <- all_data %>%
        str_subset("Mapped +") %>%
        str_split_fixed(":", 2)
    mapped_reads <- as.numeric(mapped_line[,2] %>%
                                   str_trim() %>%
                                   str_extract("^[0-9]+"))
    
    multi_align_line <- all_data %>%
        str_subset("of these") %>%
        str_split_fixed(":", 2)
    multi_align_reads <- as.numeric(multi_align_line[,2] %>%
                                        str_trim() %>%
                                        str_extract("^[0-9]+"))
    multi_align_reads_many <- as.numeric(multi_align_line[,2] %>%
                                             str_trim() %>%
                                             str_extract("[0-9]+ have >20") %>%
                                             str_extract("^[0-9]+"))
    
    sample_name <- sub('.txt', '', basename(tophat_summary_file))
    
    tophat_summary <- data_frame(sample_name = sample_name,
                                 input = input_reads,
                                 mapped = mapped_reads,
                                 multi_align = multi_align_reads,
                                 many_align = multi_align_reads_many)
    return(tophat_summary)
}


#' Parse STAR single-end alignment summary
#'
#' @param star_log_file Path to a STAR Log.final.out file
#'
#' @return A tibble with input, mapped, multi_align, and many_align read
#'   number extracted from the summary
#' 
#' @examples
#' star_se_alignment_summary('star.Log.final.out')
#' 
star_se_alignment_summary <- function(star_log_file) {
    library(readr)
    library(tidyr)
    library(stringr)
    library(tibble)
    
    all_data = read_lines(star_log_file)
    input_line <- all_data %>% 
        str_subset("Number of input reads") %>%
        str_split_fixed("\\|", 2)
    input_reads <- as.numeric(str_trim(input_line[,2]))
    
    uniquely_mapped_line <- all_data %>%
        str_subset("Uniquely mapped reads number") %>%
        str_split_fixed("\\|", 2)
    uniquely_mapped_reads <- as.numeric(uniquely_mapped_line[,2] %>%
                                            str_trim() %>%
                                            str_extract("^[0-9]+"))
    
    multi_align_line <- all_data %>%
        str_subset("Number of reads mapped to multiple loci") %>%
        str_split_fixed("\\|", 2)
    multi_align_reads <- as.numeric(multi_align_line[,2] %>%
                                        str_trim() %>%
                                        str_extract("^[0-9]+"))
    multi_align_many_line <- all_data %>%
        str_subset("Number of reads mapped to too many loci") %>%
        str_split_fixed("\\|", 2)
    multi_align_reads_many <- as.numeric(multi_align_many_line[,2] %>%
                                             str_trim() %>%
                                             str_extract("^[0-9]+"))
    
    sample_name <- sub('.Log.final.out', '', basename(star_log_file))
                                 
    summary <- data_frame(sample_name = sample_name,
                          input = input_reads,
                          mapped = uniquely_mapped_reads + multi_align_reads + 
                              multi_align_reads_many,
                          uniquely_mapped = uniquely_mapped_reads,
                          multi_align = multi_align_reads,
                          many_align = multi_align_reads_many)
    return(summary)
}



#' Aligned reads barplot
#'
#' @param alignment_stats Data frame of alignment stats: input, mapped, 
#'   multi_align, and many_align read number
#'
#'
#' @return A ggplot bar graph object
#' 
#' @examples
#' alignment_plot <- aligned_reads_barplot(alignment_stats)
#' 
aligned_reads_barplot <- function(alignment_stats, minimum_total_reads=1000000) {
    library(dplyr)
    library(tibble)
    library(ggplot2)
    library(RColorBrewer)
    library(scales)
    
    read_counts <- alignment_stats %>%
        select(sample_name, reads_total=input, mapped_reads=mapped) %>%
        mutate(unmapped_reads=reads_total - mapped_reads) %>%
        mutate(mapping_fraction=mapped_reads/reads_total)
    #levels(read_counts$sample_name) <- sampleTable$sample_name
    #cell_table <- table(read_counts$sample_name)
    sample_order <- lapply(read_counts %>% arrange(desc(reads_total)) %>% select(sample_name), as.character)
    read_counts$sample_name <- factor(read_counts$sample_name, levels = sample_order$sample_name)
    percent_mapped <- c(percent(read_counts$mapping_fraction),
                        rep("", dim(read_counts)[1]))
    reads_barplot <- ggplot(data = read_counts %>% 
                                gather(variable, count, -sample_name) %>%
                                filter(variable %in% c('unmapped_reads', 'mapped_reads')),
                            aes(x=sample_name, y=count, 
                                fill=factor(variable, levels = c('unmapped_reads', 'mapped_reads')))) +
        #fill=reorder(variable, c('mapped_reads', 'unmapped_reads')))) +
        geom_bar(stat="identity") +
        scale_fill_manual(name=element_blank(),
                          values=c( '#999999', brewer.pal(name='Blues',n=3)[3]),
                          labels=c("Unmapped", "Mapped")) +
        #scale_fill_discrete(breaks=c("Total Reads", "Mapped Reads")) +
        scale_y_continuous(labels=scales::comma, name="Number of reads") +
        scale_x_discrete(name="Sample") +
        geom_hline(yintercept=minimum_total_reads, linetype = 'dashed') +
        geom_text(label=percent_mapped) +
        theme(axis.text.x = element_text(angle=-90, vjust=1, hjust=0, size=12),
              axis.title.x = element_text(size=16),
              axis.title.y = element_text(size=16),
              plot.title = element_text(size=20),
              legend.position = c(.93, .90)) +
        ggtitle("Reads aligned per sample")
    return(reads_barplot)
}

#' Generate a trimming summary table from a directory of trimming logs
#'
#' @param trimming_log_files List of the trimming log files to summarize
#' 
#' @param trimmer The trimming tool used (currently supports trimmomatic)
#' 
#' @param suffix Optional suffix to strim from filename to generate sample_name
#'
#' @return A tibble with sample, input, retained, and dropped
#'     read numbers extracted from the summary
#' 
#' @examples
#' trimmomatic_log_files <- Sys.glob(file.path('results/trimmomatic/', "*.log"))
#' trimming_summary_table <- trimming_summary(trimmomatic_log_files, 'trimmomatic')
trimming_summary <- function(trimming_log_files, format, suffix=NA) {
    if (format == 'trimmomatic') {
        func = trimmomatic_summary
    } else {
        stop(sprintf('Unknown format specified: %s', format))
    }
    trimming_summary_table <- cbind(
        do.call(rbind, lapply(trimming_log_files, func, suffix=suffix)))
    return(trimming_summary_table)
}


#' Parse Trimmomatic log
#'
#' @param trimmomatic_log Path to a trimmomatic log file
#'
#' @param suffix Optional suffix to strim from filename to generate sample_name
#'
#' @return A tibble with sample, input, retained, and dropped
#'     read numbers extracted from the summary
#' 
#' @examples
#' trimmomatic_summary('trimmomatic.log')
#' 
trimmomatic_summary <- function(trimmomatic_log, suffix=NA) {
    library(readr)
    library(stringr)
    library(tibble)
    
    all_data = read_lines(trimmomatic_log)
    input_line <- all_data %>% 
        str_subset("Input Reads:")
    fields <- str_match(input_line, 'Input Reads: (\\d+) Surviving: (\\d+) .* Dropped: (\\d+).*')
    input_reads <- as.numeric(fields[[2]])
    retained_reads <- as.numeric(fields[[3]])
    dropped_reads <- as.numeric(fields[[4]])
 
    if (is.na(suffix)) {
        sample_name = basename(tools::file_path_sans_ext(trimmomatic_log))
    } else {
        sample_name <- sub(suffix, '', basename(trimmomatic_log))
    }
 
    trimmomatic_summary <- data_frame(sample_name = sample_name,
                                 input = input_reads,
                                 retained = retained_reads,
                                 dropped = dropped_reads)
    return(trimmomatic_summary)
}

#' Render RSeQC GeneBodyCoverage plots inline
#'
#' @param script R script output from RSeQC's geneBodyCoverage.py
#'
#' @return
#'
#' @examples
#' gene_body_coverage("56_cells_H527YBCXY")
#' 
gene_body_coverage <- function(script) {
    if (file.exists(script)) {
        gene_body_coverage_script <- system2("sed", sprintf(
            " -E '/^pdf|^dev\\.off/ s/^/#/' \"%s\"",
            script), stdout=TRUE)
        eval(parse(text=gene_body_coverage_script))
    } else {
        stop(sprintf("Unable to find file: '%s'", script))
    } 
}

#' Aligned reads barplot
#'
#' @param alignment_stats Data frame of alignment stats: input, mapped, 
#'   multi_align, and many_align read number
#'
#' @return A ggplot bar graph object
#' 
#' @examples
#' trimmed_reads_plot <- trimmed_reads_barplot(trimming_stats)
#' 
trimmed_reads_barplot <- function(trimming_stats) {
    library(dplyr)
    library(tibble)
    library(ggplot2)
    library(RColorBrewer)
    library(scales)
    
    read_counts <- trimming_stats %>%
        mutate(retained_fraction=retained/input)
    sample_order <- lapply(read_counts %>% arrange(desc(input)) %>% select(sample_name), as.character)
    read_counts$sample_name <- factor(read_counts$sample_name, levels = sample_order$sample_name)
    percent_mapped <- c(percent(read_counts$retained_fraction),
                        rep("", dim(read_counts)[1]))
    reads_barplot <- ggplot(data = read_counts %>% 
                                gather(variable, count, -sample_name) %>%
                                filter(variable %in% c('dropped', 'retained')),
                            aes(x=sample_name, y=count, 
                                fill=factor(variable, levels = c('dropped', 'retained')))) +
        #fill=reorder(variable, c('mapped_reads', 'unmapped_reads')))) +
        geom_bar(stat="identity") +
        scale_fill_manual(name=element_blank(),
                          values=c( '#999999', brewer.pal(name='Blues',n=3)[3]),
                          labels=c("Dropped", "Retained")) +
        #scale_fill_discrete(breaks=c("Total Reads", "Mapped Reads")) +
        scale_y_continuous(labels=scales::comma, name="Number of reads") +
        scale_x_discrete(name="Sample") +
        #geom_hline(yintercept=minimum_total_reads, linetype = 'dashed') +
        geom_text(label=percent_mapped) +
        theme(axis.text.x = element_text(angle=-90, vjust=1, hjust=0, size=12),
              axis.title.x = element_text(size=16),
              axis.title.y = element_text(size=16),
              plot.title = element_text(size=20),
              legend.position = c(.93, .90)) +
        ggtitle("Reads filtered per sample")
    return(reads_barplot)
}



#' Render Sample QC
#'
#' @param experiment_dir Subdirectory of `data` directory that contains
#'   featureCounts/
#'   featureCounts_summary/
#'   tophat_alignment_summary/
#'   Gene_Body_Coverage.R
#' @param minimum_total_reads Minimum number of reads required to include a
#'   sample
#'
#' @return
#'
#' @examples
#' renderSampleQC("56_cells_H527YBCXY")
#' 
renderSampleQC <- function(experiment_dir, minimum_total_reads=1000000, 
                           sample_name_regex = '([0-9]+-[0-9]+)-(.+)') {
    rmarkdown::render('sample_qc.Rmd',
                      output_dir = file.path("results", experiment_dir),
                      envir=new.env(),
                      params = list(
                          experiment_dir = experiment_dir,
                          minimum_total_reads = minimum_total_reads,
                          sample_name_regex = sample_name_regex
                      ))
}

#' Render Markdown Template
#'
#' @param template RMarkdown template file to render
#' @param experiment_dir Subdirectory of `data` directory that contains
#'   featureCounts/
#'   featureCounts_summary/
#'   tophat_alignment_summary/
#'   Gene_Body_Coverage.R
#' @param minimum_total_reads Minimum number of reads required to include a
#'   sample
#' @param sample_name_regex Regular expression used to parse sample name
#'
#' @return
#'
#' @examples
#' renderMarkdownTemplate("56_cells_H527YBCXY")
#' 
renderMarkdownTemplate <- function(template, experiment_dir, minimum_total_reads=1000000, 
                           sample_name_regex = '([0-9]+-[0-9]+)-(.+)') {
    rmarkdown::render(template,
                      output_dir = file.path("results", experiment_dir),
                      envir=new.env(),
                      params = list(
                          experiment_dir = experiment_dir,
                          minimum_total_reads = minimum_total_reads,
                          sample_name_regex = sample_name_regex
                      ))
}
