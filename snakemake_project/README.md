# PROJECT NAME

PROECT DESCRIPTION

The workflow is written using [Snakemake](https://snakemake.readthedocs.io/).

Dependencies are installed using [Bioconda](https://bioconda.github.io/) where possible.

## Setup environment and run workflow

1.  Clone workflow into working directory

    ```
    git clone <repo> <dir>
    cd <dir>
    ```

2.  Download input data

    Copy data from [URL]() to `data` directory

3.  Edit config as needed

    ```
    cp config.yaml.sample config.yaml
    nano config.yaml
    ```

4.  Install dependencies into isolated environment

    ```
    conda env create -n <project> --file environment.yaml
    ```

5.  Activate environment

    ```
    source activate <project>
    ```

6.  Execute workflow

    ```
    snakemake -n
    ```


## Running workflow on `gen-comp1`

```
snakemake --cluster-config cetus_cluster.yaml \
          --drmaa " --cpus-per-task={cluster.n} --mem={cluster.memory} --qos={cluster.qos}" \
          --use-conda -w 60 -rp -j 1000
```
