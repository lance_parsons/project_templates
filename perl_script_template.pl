#!/usr/bin/perl -w

=head1 NAME

# TODO script.pl - description


=head1 SYNOPSIS

# TODO
script.pl arguments [--help]

=head1 ARGUMENTS

=over 8

=item B<argument>

Argument

=back 

=head1 OPTIONS

=over 8

=item B<-help>

Print a brief help message and exit.

=back

=head1 DESCRIPTION

TODO Description

=head1 AUTHOR

Lance Parsons - L<http://www.linkedin.com/in/lparsons>

=head1 LICENSE

This script is in the public domain, free from copyrights or restrictions.

=cut

use strict;

# Standard Modules
use Carp;
use Getopt::Long;
use Pod::Usage;

my $opt_debug   = 0;
my $opt_help = 0;
my $opt_man = 0;
my $opt_versions = 0;
my $VER = 0.1;

GetOptions(
	'debug'   => \$opt_debug,
	'help'     => \$opt_help,
	'man'      => \$opt_man,
	'versions' => \$opt_versions,
) or pod2usage(-verbose => 1, -message => "Invalid options specified") && exit;
pod2usage(-verbose => 1) && exit if $opt_help;
pod2usage(-verbose => 2) && exit if $opt_man;

&check_opts();

END{
	if($opt_versions){
		print
		"\nModules, Perl, OS, Program info:\n",
		"  Pod::Usage            $Pod::Usage::VERSION\n",
		"  Getopt::Long          $Getopt::Long::VERSION\n",
		"  strict                $strict::VERSION\n",
		"  Perl                  $]\n",
		"  OS                    $^O\n",
		"  $0                    $VER\n",
		"\n\n";
	}
}



# Check for problem with the options or if user requests help
sub check_opts {

	# Required argument
	my $numArgs = $#ARGV + 1;
	if ( $numArgs < 1 ) {
		pod2usage(
			-exitval => 2,
			-verbose => 1,
			-message => "Required argument not specified\n"
		);
	}
}
